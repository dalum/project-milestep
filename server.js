var fs = require('fs');
var path = require('path');
var express = require('express');
var panelStatic = require('panel-static');

app = express();

app.use(panelStatic(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.listen(3000);
console.log('Server started: http://localhost:3000/');
