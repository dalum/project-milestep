/* Audio Context */
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

/* Audio Element */
var audio = new Audio();
audio.controls = true;
audio.autoplay = false;
audio.id = 'audio';

/* Analyser */
var analyser = audioCtx.createAnalyser();
analyser.fftSize = 2048;
analyser.smoothingTimeConstant = 0.5;
analyser.minDecibels = -100;
analyser.maxDecibels = 0;

/* FFT data arrays */
var bufferLength = analyser.frequencyBinCount;
var dataArray = new Float32Array(bufferLength);

/* File Selector */
input = document.createElement('input');
input.setAttribute('type', 'file');
input.id = 'input';
// input.style.opacity = 0;
// input.style.position = 'absolute';
// input.style.left = '-1000px';
input.onchange = function(e) {
    audio.src = window.URL.createObjectURL(input.files[0]);
};

/* Audio Source */
var source = audioCtx.createMediaElementSource(audio);

/* Connect */
source.connect(analyser);
analyser.connect(audioCtx.destination);

/* Visualiser Globals */
var WIDTH = window.innerWidth,
    HEIGHT = window.innerHeight,
    SPEED,
    GRAVITY = 16000;

var SPECTRUM_COLOR = function (i) {return 'rgba(50, 150, 100, 0.8)'},
    LINE_COLOR = function () {return 'rgba(50, 0, 100, ' + Math.min(0.7, Math.abs(map.l + 50) / 25) + ')'},
    BACK_COLOR = function () {x = parseInt(250 + player.x); return 'rgb('+ (x + 50) + ', ' + x + ', ' + (x + 50) + ')'};

var HIDE_CONTROLS = false;

/* Visualiser Canvas */
var canvas = document.createElement('canvas');
canvas.id = 'canvas';

var canvasCtx = canvas.getContext("2d");
canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

var info = document.createElement('span');

/* Appearance and Size */
function setSize() {
    canvas.width = WIDTH;
    canvas.height = HEIGHT;
    background.set();
    map.set();
}

function toggleControls() {
    HIDE_CONTROLS = !HIDE_CONTROLS;
    vis = HIDE_CONTROLS ? 'hidden' : 'visible';
    audio.style.visibility = vis;
    input.style.visibility = vis;
}
canvas.addEventListener('click', toggleControls);

/* Camera */
function Camera(x, v) {
    this.x = x;
    this.v = v;

    this.tx = 0;

    this.update = function () {
        this.tx += this.v * Math.sign(player.x - this.tx) / SPEED;
        if (player.x - this.tx < this.v)
            this.tx = player.x;
        this.x = (HEIGHT / 2 + this.tx);
    }

    this.rel = function (x) {
        return this.x - x;
    }
}

/* Player */
function Player(x) {
    this.x = x;
    this.v = 0;

    this.update = function () {
        if (this.x <= map.x)
            this.v = (map.x - this.x) * SPEED;
        else
            this.v -= GRAVITY * Math.min(1, 1 / (1 - map.slope)) / SPEED;
        this.x += this.v / SPEED;
        this.x = Math.max(this.x, map.x);
    };
}

/* Map */

function Map(lf = 100, rf = 500) {
    this.lf = lf;
    this.rf = rf;

    this.l = 0;
    this.r = 0;
    this.x = 0;
    this.y = 0;
    this.slope = 0;
    this.fac = 1000;

    this.spacing = 200;

    this.n;

    this.set = function () {
        this.n = HEIGHT / this.spacing + 10;
    };
    this.set();

    this.update = function () {
        for (var i = 0; i < this.rf; i++) {
            if (i < this.lf)
                this.l += dataArray[i];
            if (i < this.rf)
                this.r += dataArray[i];
        }
        this.l /= this.lf;
        this.r /= this.rf;
        this.r *= 4

        this.x = (this.l + this.r) / 2;

        this.slope = (this.r - this.l) / WIDTH;
        this.y += this.slope * this.fac / SPEED;
        this.y %= this.spacing;
    };

    this.render = function (ctx) {
        for (var i = -this.n / 2; i < this.n / 2; i++) {
            ctx.beginPath();
            ctx.moveTo(-this.spacing, camera.rel(-this.y + i * this.spacing));
            ctx.lineTo(WIDTH + this.spacing, camera.rel(-this.y + i * this.spacing
                                                        - (WIDTH + 2 * this.spacing) * this.slope));
            ctx.lineWidth = this.spacing / 2;
            ctx.strokeStyle = LINE_COLOR();
            ctx.stroke();
        }
    };
}

/* Background */
function Background() {
    this.bw, this.n;

    this.set = function () {
        this.bw = (WIDTH / bufferLength) * 4;
        this.n = Math.ceil(WIDTH / this.bw);
    };
    this.set();

    this.render = function (ctx) {
        for (var i = 0; i < this.n; i++) {
            ctx.fillStyle = SPECTRUM_COLOR();
            ctx.fillRect(i * this.bw,
                         HEIGHT,
                         this.bw,
                         camera.rel(dataArray[i] * HEIGHT / 140 + HEIGHT));
        }
    }
}

/* Update */
function update() {
    analyser.getFloatFrequencyData(dataArray);
    map.update();
    player.update();
    camera.update();
}
// setInterval(update, 1000 / SPEED);

/* Drawing */
var _t;

function draw() {
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
    setSize();
    SPEED = 1000 / (Date.now() - _t);
    _t = Date.now();

    drawVisual = requestAnimationFrame(draw);
    update();
    render(canvasCtx);
};

function render(ctx) {
    ctx.fillStyle = BACK_COLOR();
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    background.render(ctx);
    map.render(ctx)
};

/* Setup */
document.body.appendChild(canvas);
document.body.appendChild(audio);
document.body.appendChild(input);
document.body.appendChild(info);

camera = new Camera(0, 150);
player = new Player(0);
map = new Map();
background = new Background();

_t = Date.now();
setSize();
draw();
