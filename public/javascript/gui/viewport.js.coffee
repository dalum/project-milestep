Painter = require './painter.js'

class Viewport
    constructor: (@canvas) ->
        @matchSize()

        @ctx = @canvas[0].getContext('2d')
        @painter = new Painter(@canvas, @ctx)

    # Set the size of the viewport element
    setSize: (width, height) ->
        @canvas[0].width = width
        @canvas[0].height = height

    # Match the actual size with the jQuery computed value    
    matchSize: ->
        @setSize(@canvas.width(), @canvas.height())

    # Match the actual size with the jQuery computed value    
    fillWindow: ->
        @setSize(window.innerWidth, window.innerHeight)

    draw: (fps) =>
        @render()
        setTimeout(
            =>
                requestAnimationFrame( => @draw(fps))
            , 1000 / fps)

    render: ->
        @painter.setFillColor('rgb(0, 0, 0)')
        @painter.setStrokeColor('rgb(255, 255, 255)')
        @painter.setLineWidth(5)

        @painter.clear()
        @painter.line(0, 0, @canvas.width(), @canvas.height())
        @painter.line(0, @canvas.height(), @canvas.width(), 0)

module.exports = Viewport
