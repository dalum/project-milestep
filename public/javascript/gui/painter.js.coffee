class Painter
    constructor: (@canvas, @ctx) ->

    setLineWidth: (lineWidth) ->
        @ctx.lineWidth = lineWidth

    setStrokeColor: (color) ->
        @ctx.strokeStyle = color

    setFillColor: (color) ->
        @ctx.fillStyle = color

    clear: ->
        @ctx.clearRect(0, 0, @canvas.width(), @canvas.height());

    fill: ->
        @ctx.fillRect(0, 0, @canvas.width(), @canvas.height());

    line: (x1, y1, x2, y2) ->
        @ctx.beginPath()
        @ctx.moveTo(x1, y1)
        @ctx.lineTo(x2, y2)
        @ctx.stroke()

module.exports = Painter
