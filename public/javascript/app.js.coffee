Viewport = require './gui/viewport.js'
$ = require 'jquery'

init = ->
    viewport = new Viewport $("#viewport")
    viewport.fillWindow()
    $(window).bind('resize', viewport.fillWindow)
    viewport.draw(30)

window.onload = init
