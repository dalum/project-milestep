# Requirements

* npm
* Node.js (included in npm on Arch Linux)

# Installation

$ npm install

# Run

$ npm start

The app should now be running and accesible in your browser at
http://localhost:3000.

Happy hacking!